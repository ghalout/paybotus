def addToContextAction(value, context):
    key = value["key"]
    if key[0] != "$":
        key = "$"+key

    context[key] = value["value"]
    return context

# pylint: disable=unused-argument
def addEntitytocontext(value, context):
    data = {}
    for key in context:
        if "@" in key:
            entityKey = key.replace('@', '$')
            data[entityKey] = context[key]

    return data
