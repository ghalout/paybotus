#pylint: disable=unused-argument
import time
import json
import logging
from datetime import datetime
import jwt
import requests
from dataAccessLayer.dbHandler import insertChatlogsTrack
from store.dataStore import DataStore
from logger.performancelogger import Performancelogger

LOGGER = logging.getLogger(__name__)


def toCurrency(value):
    return "$" + value


def getROTPConfig(billerId):
    return DataStore.getBillerChatbotSetting(billerId)["rotp"]


def getAuthHeader(billerId):
    header = {
        "Authorization": createToken(billerId)
    }
    return header


def generateJWTToken(billerId, xotpKey):
    jwtTkn = jwt.encode(
        {
            "iss": billerId,
            "iat": int(time.time()),
            "requestedScope": ["xotp"],
            "requestedTTL": 300
        },
        key=xotpKey,
        algorithm='HS256',
        headers={
            "typ": "JWT",
            "kid": "001"
        }
    )
    return jwtTkn


def createToken(billerId):
    rotpConfig = getROTPConfig(billerId)
    url = "{0}/api/token/{1}".format(rotpConfig["url"], billerId)
    params = {
        "jwt": generateJWTToken(billerId, rotpConfig["xotp_key"])
    }
    headers = {
        "content-Type": "application/x-www-form-urlencoded"
    }
    response = requests.post(url, data=params, headers=headers)
    data = response.json()
    token = data["token"]
    return token


def setData(response, status):
    date = response["payment-date"]
    datetimeobject = datetime.strptime(date, '%m%d%Y%H%M%S')
    newformat = datetimeobject.strftime('%m-%d-%Y')
    data = {
        "$referencenumber": str(response["reference-number"]),
        "$paymentamount": toCurrency(str(response["payment-amount"])),
        "$totalamount": toCurrency(str(response["total-amount"])),
        "$paystatus": str(status),
        "$accountnumber": response["account-number"],
        "$convenienceFee": str(response["convenience-fee"]),
        "$paymentDate": newformat
    }
    return data

# TODO: refactor: Function name not clear


def clearList(context):
    if "$accountNumberList" in context:
        del context["$accountNumberList"]
        del context["$amountDueList"]
        del context["$statusList"]
        #del context["$acclength"]

# TODO: refactor: Function name not clear


def handleError(response):
    messages = []
    error = response["errors"]["error"]
    if isinstance(error, list):
        for err in error:
            messages.append(str(err["value"]))
    else:
        messages.append(str(error["value"]))
    message = ",".join(map(str, messages))
    # TODO: why here are multiple message in data
    data = {
        "$accountnumber": response["account-number"],
        "$status": "FAILED",
        "~message": message,
        "$errormessage": message
    }
    return data

# TODO: refactor function name


def apiCall(params, billerId, payment):
    authHeader = getAuthHeader(billerId)
    headers = {
        "content-Type": "application/json",
        **authHeader
    }
    rotpConfig = getROTPConfig(billerId)
    url = "{0}/api/v2/{1}/{2}".format(
        rotpConfig["url"],
        payment,
        billerId
    )
    response = requests.post(url, data=json.dumps(params), headers=headers)
    data = response.json()
    return data

# TODO: refactor


def getApiResponse(context, additionalParams):
    headerlist = []
    billerId = context["$tla"]

    for index in range(0, len(context["$accountNumberList"])):
        header = {
            "operation": "SALE",
            "account-number": int(context["$accountNumberList"][index]),
            "auth-token1": 12345,
            "payment-amount": float(context["$amountDueList"][index][1:]),
            "payment-type-code": "UTILITY",
            "check-duplicates": True
        }
        if "$paydate" in context:
            datepay = context["$paydate"][0]
            header['payment-date'] = {"date-str": datepay}
        headerlist.append(header)

    params = {
        "payment": {
            "header": headerlist,
            "customer": {
                "first-name": context["$firstName"][0],
                "last-name": context["$lastName"][0],
                "day-phone-nr": int(context["$phone"][0]),
                "email": context["$email"][0],
                "address": {
                    "line1": "",
                    "state": "",
                    "zip-code": context["$zipCode"][0],
                    "city": "",
                    "country": ""
                }
            }
        }
    }
    params["payment"]["payment-method"] = additionalParams
    data = apiCall(params, billerId, "payments")
    clearList(context)
    contextData = setContextDatagetApiResponse(data, context)
    return contextData


def setContextDatagetApiResponse(data, context):
    errorList = []
    acceptedList = []
    response = data["payment-response"]
    if "response" in response:
        for item in response["response"]:
            status = item["payment-status"].upper()
            if status == 'FAILED':
                data = handleError(item)
                errorList.append(data)
            else:
                data = setData(item, status)
                acceptedList.append(data)
        logData = {
            "conversationId" : context["$conversationId"],
            "startTime" : "",
            "payCount" : int(len(acceptedList)),
            "autoPayCount" : False
        }
        insertChatlogsTrack(logData)
        data = {
            "$accetedList": acceptedList,
            "$errorList": errorList
        }
        return data
    else:
        status = str(response["payment-status"]).upper()
        if "account-number" not in response:
            response["account-number"] = context["$accountNumber"]
        if status == 'FAILED':
            data = handleError(response)
            errorList.append(data)
        else:
            data = setData(response, status)
            acceptedList.append(data)
        logData = {
            "conversationId" : context["$conversationId"],
            "startTime" : "",
            "payCount" : int(len(acceptedList)),
            "autoPayCount" : False
        }
        insertChatlogsTrack(logData)
        data = {
            "$accetedList": acceptedList,
            "$errorList": errorList
        }
    return data


def calsum(data):
    sumNo = sum([float(i) for i in data if isinstance(i, str) or i.isdigit()])
    return round(sumNo, 2)


def restApiAccountEnquiry(inputdata, context):
    billerId = context["$tla"]
    rotpConfig = DataStore.getBillerChatbotSetting(billerId)["rotp"]
    url = rotpConfig["url"]
    if '$accountNumber' in context:
        accountNumber = context["$accountNumber"]
        context["@sys-number"] = [accountNumber]
    else:
        accountNumber = context["@sys-number"][0]

    performanceLogger = Performancelogger.start()
    headers = getAuthHeader(billerId)
    url = "{0}/api/v2/accounts/{1}?payment-type-code={2}&auth-token1={3}&account-number={4}"\
        .format(
            url,
            billerId,
            "UTILITY",
            12345,
            accountNumber
        )
    response = requests.get(url, headers=headers)
    performanceLogger.stop(context["$conversationId"], "Account Enquiry api")
    data = response.json()
    contextData = setContextVariablerestApiAccountEnquiry(data, context)
    return contextData


def setContextVariablerestApiAccountEnquiry(data, context):
    amountDueList = []
    accountNumberList = []
    statusList = []
    if "$amountDueList" in context:
        amountDueList = context["$amountDueList"]
        accountNumberList = context["$accountNumberList"]
        statusList = context["$statusList"]
    response = data["account-info-response"]
    status = str(response["validation-status"]).upper()

    if status == "PASSED":
        tiemVar = response["date-due"]
        dtObject = datetime.fromtimestamp(int(tiemVar))
        dueDate = dtObject.strftime("%m/%d/%Y")

        if response["amount-due"] != 0:
            amountDueList.append(str(response["amount-due"]))
            accountNumberList.append(context["@sys-number"][0])
            statusList.append(str(status))

        data = {
            "$billAmount": toCurrency(str(response["amount-due"])),
            "$duedate": dueDate,
            "$accountNumber": context["@sys-number"][0],
            "$status": str(status),
            "$accountNumberList": accountNumberList,
            "$amountDueList": amountDueList,
            "$statusList": statusList,
            "$acclength": str(len(accountNumberList)),
            "$totalAmount": calsum(amountDueList)
        }
    else:
        response["account-number"] = context["@sys-number"][0]
        data = handleError(response)

    return data


def getAccountNoByEmail(inputdata, context):
    billerId = context["$tla"]
    rotpConfig = DataStore.getBillerChatbotSetting(billerId)["rotp"]
    url = rotpConfig["url"]
    emailId = context["@emailAddress"][0]
    performanceLogger = Performancelogger.start()
    headers = getAuthHeader(billerId)
    url = "{0}/api/v2/accountInfo/email/{1}/{2}"\
        .format(
            url,
            billerId,
            emailId
        )
    response = requests.get(url, headers=headers)
    performanceLogger.stop(
        context["$conversationId"], "Get account no by email ID")
    data = response.json()
    contextData = setContextDataGetAccountNoByEmail(data, context)
    return contextData


def setContextDataGetAccountNoByEmail(data, context):
    if "client-account" in data["list-account-info-facades-response"]:
        data = {
            "$status": "SUCCESS",
            "$accountNo": data["list-account-info-facades-response"]\
                ["client-account"]["account-number"]
        }
    else:
        data = {
            "$status": "FAILED",
            "~message": "Currently there is no account associated with this email."
        }
    return data


def makePayment(inputdata, context):
    performanceLogger = Performancelogger.start()
    expiryDate = context["$exp"][0].split("/")
    additionalParams = {
        "account-number": int(context["$cardNumber"][0]),
        "type": "VISA",
        "card-holder-name": context["$cardHolderName"][0],
        "cvv": int(context["$cvv"][0]),
        "credit-card-expiry-date": {
            "month": expiryDate[0],
            "year": int(expiryDate[1])
        }
    }
    data = getApiResponse(context, additionalParams)
    performanceLogger.stop(
        context["$conversationId"], "Make Payment Without Echeck api")
    return data


def extractDay(context):
    date = context["$duedate"].split("/")
    duedate = date[0]
    return duedate


def makeAutoPayment(inputdata, context):
    billerId = context["$tla"]
    params = {
        "payment-schedule": {
            "action": "C",
            "header": {
                "account-number": int(context["$accountNumber"][0]),
                "payment-type-code": "UTILITY",
                "check-duplicates": True,
                "schedule-day": int(extractDay(context)),
                "schedule-type-code": "MONTHLY_BILL_AMOUNT"
            },
            "payment-method": {
                "token": "B1D5781111D84F7B3FE45A0852E59758CD7A87E5"
            },
            "customer": {
                "first-name": context["$firstName"][0],
                "last-name": context["$lastName"][0],
                "day-phone-nr": int(context["$phone"][0]),
                "email": context["$email"][0],
                "address": {
                    "line1": "",
                    "state": "",
                    "zip-code": context["$zipCode"][0],
                    "city": "",
                    "country": ""
                }
            }
        }
    }
    data = apiCall(params, billerId, "autopay")
    contextData = setContextVariablemakeAutoPayment(data, context)
    return contextData


def setContextVariablemakeAutoPayment(data, context):
    response = data["payment-schedule-response"]
    status = data["payment-schedule-response"]["payment-schedule-status"].upper()
    if status == 'SCHEDULED':
        logData = {
            "conversationId" : context["$conversationId"],
            "startTime" : "",
            "payCount" : 0,
            "autoPayCount" : True
        }
        insertChatlogsTrack(logData)
        data = {
            "$autodate": int(extractDay(context))
        }
    else:
        if "account-number" not in response:
            response["account-number"] = context["$accountNumber"]
        data = handleError(response)
    return data


def makePaymentEcheck(inputdata, context):
    performanceLogger = Performancelogger.start()
    additionalParams = {
        "routing-number": context["$routingNumber"][0],
        "fi-name": context["$bankName"][0],
        "type": "CHQ",
        "account-number": context["$accountNumbercheck"][0]
    }
    data = getApiResponse(context, additionalParams)
    performanceLogger.stop(
        context["$conversationId"], "Make Payment Echeck api")
    return data


def viewBillDetails(inputdata, context):
    performanceLogger = Performancelogger.start()
    billerId = context["$tla"]
    rotpConfig = DataStore.getBillerChatbotSetting(billerId)["rotp"]
    url = rotpConfig["url"]
    headers = getAuthHeader(billerId)
    url = "{0}/api/v2/payments/ref/{1}/{2}".format(
        url,
        billerId,
        context["$referencenumber"]
    )
    response = requests.get(url, headers=headers)
    performanceLogger.stop(context["$conversationId"], "View Bill Details api")
    data = response.json()
    contextData = setContextVariableviewBillDetails(data, context)
    return contextData


def setContextVariableviewBillDetails(data, context):
    response = data["last-payment-response"]
    status = response["payment-status"].upper()
    if status != 'FAILED':
        data = {
            "$referencenumber": str(response["reference-number"]),
            "$paymentamount": toCurrency(str(response["payment-amount"])),
            "$totalamount": toCurrency(str(response["total-amount"])),
            "$convenience-fee": toCurrency(str(response["convenience-fee"])),
            "$status": str(status)
        }
    else:
        if "account-number" not in response:
            response["account-number"] = context["$accountNumber"]
        data = handleError(response)
    clearList(context)
    return data


def restApiLastAccountEnquiry(inputdata, context):
    if "@AccountNumberForLastBills" in context:
        accountNumber = context["@AccountNumberForLastBills"][0]
    elif '$accountNumber' in context:
        accountNumber = context["$accountNumber"]
        context["@sys-number"] = [accountNumber]
    else:
        accountNumber = context["@sys-number"][0]
    performanceLogger = Performancelogger.start()
    billerId = context["$tla"]
    rotpConfig = DataStore.getBillerChatbotSetting(billerId)["rotp"]
    url = rotpConfig["url"]
    headers = getAuthHeader(billerId)
    url = "{0}/api/v2/payments/search/{1}?payment-type-code={2}&account-number={3}"\
        .format(
            url,
            billerId,
            "UTILITY",
            accountNumber
        )
    response = requests.get(url, headers=headers)
    performanceLogger.stop(
        context["$conversationId"], "Last Account Enquiry api")
    data = response.json()
    contextData = setContextVariablerestApiLastAccountEnquiry(data)
    return contextData


def setContextVariablerestApiLastAccountEnquiry(data):
    response = data["last-payment-response"]
    if "payment-status" in response:
        data = {
            "$lrefnumber": str(response["reference-number"]),
            "$lpaymentamount": toCurrency(str(response["total-amount"])),
            "$status": response["payment-status"]
        }
    else:
        data = {
            "$status": "FAILED",
            "~message": "Account Number is Invalid."
        }
    return data


def payBillContextVariable(inputdata, context):
    data = {
        "$isPayBill": "true",
        "$isViewBill": "false",
        "$isGetBalance": "false"
    }
    return data


def viewBillContextVariable(inputdata, context):
    data = {
        "$isViewBill": "true",
        "$isPayBill": "false",
        "$isGetBalance": "false"
    }
    return data


def getBalanceContextVariable(inputdata, context):
    data = {
        "$isGetBalance": "true",
        "$isViewBill": "false",
        "$isPayBill": "false"
    }
    return data
