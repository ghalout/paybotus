from RuleEngine import StringBuilder
from PaxcelChatBotFramework.Models import Output
def textResponse(inputValue, context):
    result = {
        "action": "return"
    }
    output = Output()
    output.Type = "text"
    output.Message = StringBuilder(inputValue, context)
    result["output"] = output
    return result
