from RuleEngine import ResponseManager, ConditionEvaluator, Action

def conditionalResponse(inputValue, context):
    for item in inputValue:
        evaluator = ConditionEvaluator()
        if evaluator.Eval(item["condition"], context):
            resp = item["response"]
            responses = []
            if isinstance(resp, list):
                for rsp in resp:
                    responses.append(Action(rsp))
            else:
                responses.append(Action(resp))

            output = []
            for response in responses:
                value = ResponseManager.call_action(response.Type, {
                    "data": response.Value,
                    "context": context
                })
                if response.Type == "jumpto":
                    return value
                output.append(value["output"])

            return {
                "action": "return",
                "output": output
            }
    return None
