from PaxcelChatBotFramework.Models import Output
from RuleEngine import StringBuilder
def optionResponse(inputValue, context):
    options = []
    text = inputValue["text"]
    option = inputValue["options"]
    if isinstance(option, str):
        if inputValue in context:
            options = context[inputValue]
        else:
            options.append(inputValue)
    elif isinstance(option, list):
        options = inputValue
    output = Output()
    output.Data = {
        "text": StringBuilder(text, context),
        "options": options
        }
    output.Type = "option"
    result = {
        "action": "return",
        "output": output
    }
    return result
