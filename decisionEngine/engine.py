import logging
import datetime

from RuleEngine import Engine as RuleExecutor
from RuleEngine import ActionManager, ResponseManager
from PaxcelChatBotFramework import Engine as ChatBotRuleEngine
from PaxcelChatBotFramework.Models import VisitedNode, RuleEngineOutput
from PaxcelChatBotFramework.Enums import RedirectType
from dataAccessLayer.dbHandler import insertChatlogs
from logger.performancelogger import Performancelogger
from .contextBuilder import contextBuilder
from .ruleManager import RuleManager

from .response import textResponse, optionResponse, \
    formResponse, conditionalResponse, jumptoResponse
from .actions import addToContextAction, addEntitytocontext, \
    restApiAccountEnquiry, restApiLastAccountEnquiry, viewBillDetails, \
    makePayment, makePaymentEcheck, makeAutoPayment, getAccountNoByEmail, \
    payBillContextVariable, viewBillContextVariable, getBalanceContextVariable


LOGGER = logging.getLogger(__name__)


class DecisionEngine(ChatBotRuleEngine):
    @staticmethod
    def initialize():
        ResponseManager.add_action("text", textResponse)
        ResponseManager.add_action("options", optionResponse)
        ResponseManager.add_action("form", formResponse)
        ResponseManager.add_action("conditional", conditionalResponse)
        ResponseManager.add_action("jumpto", jumptoResponse)

        ActionManager.add_action("addToContext", addToContextAction)
        ActionManager.add_action("AccountEnquiry", restApiAccountEnquiry)
        ActionManager.add_action("ViewBillDetails", viewBillDetails)
        ActionManager.add_action("PayBill", makePayment)
        ActionManager.add_action(
            "restApiLastAccountEnquiry", restApiLastAccountEnquiry)
        ActionManager.add_action("addEntitytocontext", addEntitytocontext)
        ActionManager.add_action("makePaymentEcheck", makePaymentEcheck)
        ActionManager.add_action("makeAutoPayment", makeAutoPayment)
        ActionManager.add_action("getAccountNoByEmail", getAccountNoByEmail)
        ActionManager.add_action("payBillContextVariable", payBillContextVariable)
        ActionManager.add_action("viewBillContextVariable", viewBillContextVariable)
        ActionManager.add_action("getBalanceContextVariable", getBalanceContextVariable)



    def __init__(self):
        pass

    def Process(self, botResponse):
        try:
            performanceLogger = Performancelogger.start()
            dbConfig = self.__fetchDBValues__(botResponse.AdditionalData)
            ruleLoader = RuleManager(dbConfig)
            ruleExec = RuleExecutor(ruleLoader)
            contextTable = contextBuilder(botResponse)
            resp = ruleExec.Execute(contextTable)
            result = RuleEngineOutput()
            result.AdditionalData = botResponse.AdditionalData
            result.Context = self.__prepareContext__(
                botResponse.Context, resp["context"])
            result.Output = self.__prepareOutput__(resp["output"])
            intent = result.Context.VisitedNodes[-1]
            dialogName = intent.Name
            logData = {
                "conversationId" : botResponse.Context.ConversationId,
                "inputMessage" : botResponse.AdditionalData["message"]["message"],
                "startTime" : datetime.datetime.utcnow(),
                "isBot" : False,
                "dialogName" : dialogName
            }
            insertChatlogs(logData)

            performanceLogger.stop(
                botResponse.Context.ConversationId, "Rule Engine")
            self.__handleDestinationBot__(result, ruleLoader)
        except:
            LOGGER.info(
                "User Query: %s",
                "Exception occurd in rule engine"
            )
            raise
        return result

    def __fetchDBValues__(self, additionalData):
        config = {
            "message": additionalData["message"],
            "uuid": additionalData["conversationId"],
            "billerId": additionalData["tla"]
        }
        return config

    def __prepareContext__(self, oldContext, engineContext):
        nodes = engineContext["nodes"]
        oldContext.Properties = {}
        oldContext.VisitedNodes = []
        for node in nodes:
            vnode = VisitedNode()
            if isinstance(node, VisitedNode):
                vnode = node
                vnode.RedirectType = self.__getRedirectType__(
                    vnode.RedirectType)
            else:
                vnode.NodeId = node["nodeid"]
                vnode.Name = node["name"]
                vnode.RedirectType = self.__getRedirectType__(
                    node["redirectType"])

            oldContext.VisitedNodes.append(vnode)

        for prop in engineContext:
            oldContext.Properties[prop] = engineContext[prop]
        return oldContext

    def __getRedirectType__(self, redirectType):
        if redirectType == "slot":
            return RedirectType.SLOT
        # if redirectType == "return":
        return RedirectType.RESPONSE

    def __prepareOutput__(self, responses):
        result = []
        for response in responses:
            result.append(response)

        return result

    def __handleDestinationBot__(self, response, ruleloader):
        lastNode = response.Context.VisitedNodes[-1]
        if not ruleloader.hasChildNode(lastNode.NodeId):
            response.Context.Properties["destination_bot"] = "agent"
