def contextBuilder(botResponse):
    table = {
        "$nodes": []
    }
    if botResponse.Intent is not None:
        table["#intent"] = botResponse.Intent.Name
    for entity in botResponse.Entities:
        entityName = "@" + entity.Name
        if (entityName) not in table:
            table[entityName] = []
        table[entityName].append(entity.Value)

    context = botResponse.Context
    table["$nodes"] = context.VisitedNodes
    for key in context.Properties.keys():
        table['$' + str(key)] = context.Properties[key]

    table["$tla"] = botResponse.AdditionalData["tla"]
    table["$conversationId"] = botResponse.Context.ConversationId
    return table
