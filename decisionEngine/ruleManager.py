from RuleEngine.Components import RuleLoader
from dataAccessLayer.dbHandler import getRules, getRuleById, getAnyThingElseNode, hasChildNode


class RuleManager(RuleLoader):
    def __init__(self, config):
        super().__init__()
        self.config = config

    def getRules(self, parentNodeId) -> list:
        return getRules(parentNodeId, self.config["billerId"])

    def getRuleById(self, nodeid):
        return getRuleById(nodeid, self.config["billerId"])

    def getAnyThingElseNode(self):
        return getAnyThingElseNode(self.config["billerId"])

    def hasChildNode(self, parentNodeId):
        return hasChildNode(parentNodeId, self.config["billerId"])
