import logging
import json
import datetime
from bson.json_util import dumps, RELAXED_JSON_OPTIONS
from .dbConnectionPooling import getDb

LOGGER = logging.getLogger(__name__)


def getRules(parentId, billerId):
    if parentId is None:
        parentId = ""
    return getNodeList({"parent_dialog_id": parentId, "tla": billerId})


def getRuleById(ruleId, billerId):
    return getNodeList({"node_id": ruleId, "tla": billerId})[0]


def getAnyThingElseNode(billerId):
    node = getNodeList({"name": "anything_else", "tla": billerId})[0]
    return node


def hasChildNode(parentId, billerId):
    nodes = getNodeList({"parent_dialog_id": parentId, "tla": billerId})
    return len(nodes) != 0


def getNodeList(query):
    ruleList = []
    collection = getDb()["CBT_BLR_DIALOGS"]
    node = None
    for node in collection.find(query):
        ruleList.append(formatNode(node))
    return ruleList


def formatNode(node):
    formatedRule = {**node}
    formatedRule["nodeid"] = node["node_id"]
    del formatedRule["node_id"]
    del formatedRule["_id"]
    return formatedRule


def insertData(data, collectionName):
    dbName = getDb()
    collection = dbName[collectionName]
    collection.insert_one(data)


def insertChatlogs(data):
    userType = 2
    if data["isBot"]:
        userType = 1
    mydict = {
        "message": data["inputMessage"],
        "conv_id": data["conversationId"],
        "added_on": data["startTime"],
        "user_type_code": userType,
        "intent_detected": data["dialogName"]}
    insertData(mydict, "CHATLOGS")

def insertChatlogsTrack(data):
    utcDatetime = datetime.datetime.utcnow()
    mydict = {
        "conv_id": data["conversationId"],
        "added_on": utcDatetime,
        "payCount": data["payCount"],
        "autoPayCount": data["autoPayCount"]}
    insertData(mydict, "CBT_BLR_TRACK")


def insertChatlogsMaster(data):
    utcDatetime = datetime.datetime.utcnow()
    mydict = {
        "conv_id": data["conversationId"],
        "tla": data["tla"],
        "user_agent": data["headers"]["User-Agent"],
        "ip_address": "",
        "headers": data["headers"],
        "added_on": utcDatetime,
        "browser": "",
        "os": "",
        "visitor" : {
            "firstName" : data["visitor"]["firstName"],
            "lastName" : data["visitor"]["lastName"]
            }
        }
    insertData(mydict, "CHATLOGS_CLIENTREQUEST")


def insertFeedback(data):
    mydict = {
        "message": data["inputMessage"],
        "conv_id": data["conversationId"],
        "tla": data["user"],
        "added_on": data["startTime"]}
    insertData(mydict, "CBT_FEEDBACKS")


def getBillerSettings(billerId):
    collection = getDb()["BLR_SETTINGS"]
    setting = collection.find_one({"tla": billerId})
    setting = json.loads(
        dumps(
            setting,
            json_options=RELAXED_JSON_OPTIONS
        )
    )
    return setting
