import pymongo
__connection__ = {}


def initializeDB(config):
    url = config.get("url", "mongodb://localhost:27017")
    # port = config.get("port", 27017)
    maxPoolSize = config.get("maxPoolSize", 10)
    idleTimeout = config.get("idleTimeout", 100)
    client = pymongo.MongoClient(
        url,
        
        maxPoolSize=maxPoolSize,
        waitQueueTimeoutMS=idleTimeout
    )
    dbName = config.get("dbName", "PayBotUs")
    __connection__["mongo"] = client[dbName]

def getMongoDb():
    return __connection__["mongo"]

def getDb():
    return getMongoDb()
