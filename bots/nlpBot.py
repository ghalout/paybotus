import json
import logging
from PaxcelChatBotFramework import Bot
from PaxcelChatBotFramework.Models import BotOutput, Intent, Entity
from NLPEngineCore import EngineInterface
from logger.performancelogger import Performancelogger
from .watsonAssistant import Assistant

LOGGER = logging.getLogger(__name__)


class NLPBot(Bot):
    __assistant__ = None
    init = False

    def getName(self):
        return "NLP Bot"
    Name = property(getName)
    @staticmethod
    def initialize(config):
        NLPBot.__assistant__ = Assistant(config)
        EngineInterface.Initialize({
            "assistant": NLPBot.__assistant__.message,
            "getDestinationBot": NLPBot.getDestinationBot
        })
        NLPBot.init = True

    @staticmethod
    def CanHandle(request):
        LOGGER.info(
            "User Query: %s",
            "type message received"
        )
        return request.Type == "message"

    @staticmethod
    def getDestinationBot(context):
        try:
            LOGGER.info(
                "User Query: %s",
                'now we Get DestinationBot'
            )
            destinationBot = "AGENT"
            botWorkspaceIDs = {}
            if "workspaceIds" in context:
                botWorkspaceIDs = context["workspaceIds"]
            if (
                    context is not None and
                    "destination_bot" in context and
                    context["destination_bot"] is not None):
                destinationBot = context["destination_bot"].upper()
            wsId = botWorkspaceIDs[destinationBot]
            if wsId is None:
                wsId = botWorkspaceIDs["AGENT"]

            if destinationBot is None:
                destinationBot = "agent"
        except Exception as ex:
            LOGGER.info(
                "User Query: %s",
                "exception in NLP Bot method GetDestinationBot:"+str(ex)
            )
            raise

        return wsId

    def __prepareWatsonContext__(self, context, additionalData):
        watsonContext = {}
        if "destination_bot" in context.Properties:
            watsonContext["destination_bot"] = context.Properties["destination_bot"]
        if "workspaceIds" in additionalData:
            watsonContext["workspaceIds"] = additionalData["workspaceIds"]
        return watsonContext

    def __init__(self):
        if not NLPBot.init:
            raise Exception("Initialize the bot before using it")

    def Process(self, request) -> BotOutput:
        LOGGER.info(
            "User Query: %s",
            "in processor function"
        )
        engine = EngineInterface()
        LOGGER.info(
            "User Query: %s",
            "engine intialized"
        )
        watsonRequest = {
            "context": self.__prepareWatsonContext__(request.Context, request.AdditionalData),
            "input": {
                "type": "text",
                "text": request.Input.Message
            }
        }
        swrequest = json.dumps(watsonRequest)
        LOGGER.info(
            "User Query: %s",
            swrequest
        )

        try:
            performanceLogger = Performancelogger.start()
            resp = engine.Execute(watsonRequest)
            performanceLogger.stop(request.Context.ConversationId, "Watson")
            LOGGER.info(
                "User Query: %s",
                "watson response"
            )
            botOutput = BotOutput()
            botOutput.AdditionalData = request.AdditionalData
            botOutput.Entities = self.__extractEntities__(
                resp["entities"],
                watsonRequest["input"]["text"]
            )
            botOutput.Intent = self.__extractIntent__(resp["intents"])
            botOutput.Context = self.__prepareOutputContext__(
                resp["context"], request.Context)
            logging.debug("Intents {botOutput.Intent.Name}")
        except Exception as ex:
            LOGGER.info(
                "User Query: %s",
                "an exception occurd in watson response:"+str(ex)
            )
            raise
        return botOutput

    def __prepareOutputContext__(self, watsonContext, originalContext):
        if "destination_bot" in watsonContext:
            originalContext.Properties["destination_bot"] = watsonContext["destination_bot"]
        return originalContext

    def __extractEntities__(self, watsonEntities, inputText):
        entities = []
        for item in watsonEntities:
            entity = Entity()
            entity.Name = item["entity"]
            entity.Value = item["value"]
            entity.Confidence = item["confidence"]
            location = item["location"]
            if len(location) > 1:
                entity.RawValue = inputText[location[0]:location[1]]
            entities.append(entity)
        return entities

    def __extractIntent__(self, watsonIntents):
        # pylint: disable=len-as-condition
        if len(watsonIntents) == 0:
            return None

        sortedList = sorted(
            watsonIntents, key=self.__sortbyconfidence__, reverse=True)
        intent = Intent()
        intent.Name = sortedList[0]["intent"]
        intent.Confidence = sortedList[0]["confidence"]
        return intent

    def __sortbyconfidence__(self, elem):
        return elem["confidence"]
