import logging
from ibm_watson import AssistantV1

LOGGER = logging.getLogger(__name__)


class Assistant:
    """ This is a class description"""

    def __init__(self, config):
        if config["type"] == "iam_apikey":
            self.__assistant__ = AssistantV1(iam_apikey=config["iam_apikey"],

                                             version=config["version"],
                                             # iam_access_token= config.apiToken,
                                             url=config["url"])
        else:
            self.__assistant__ = AssistantV1(username=config["username"],
                                             password=config["password"],

                                             version=config["version"])

    def message(self, payload):
        try:
            response = self.__assistant__.message(workspace_id=payload["workspace_id"],
                                                  input=payload["input"],
                                                  context=payload["context"]
                                                  ).get_result()
            LOGGER.info(
                "User Query: %s",
                response,
                extra=response
            )
        except Exception as ex:
            LOGGER.error(
                "User Query: %s",
                'error from watson end: '+str(ex),
                extra=response
            )
            raise

        return response
