from PaxcelChatBotFramework import Bot
from PaxcelChatBotFramework.Models import BotOutput, Intent, Entity

class TemplateBot(Bot):
    def getName(self):
        return "Tempalate Bot"
    Name = property(getName)

    @staticmethod
    def CanHandle(request):
        return request.Type == "template"

    def Process(self, request) -> BotOutput:
        output = BotOutput()
        value = request.Input
        if value.Type == "form":
            data = value.Data
            output.Intent = Intent()
            output.Intent.Name = data["formName"]
            output.Intent.Confidence = 1.0
            if "fields" not in data:
                data["fields"] = dict()
            for item in data["fields"]:
                entity = Entity()
                entity.Name = item
                entity.Value = data["fields"][item]
                entity.Confidence = 1.0
                output.Entities.append(entity)

        output.Context = request.Context
        output.AdditionalData = request.AdditionalData

        return output
