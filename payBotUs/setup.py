from PaxcelChatBotFramework import Program, ComponentManager
from inputProcessor import WebInputProcessor
from outputProcessor import WebOutputProcessor
from bots import TemplateBot, NLPBot
from decisionEngine import DecisionEngine
from store import DataStore
from dataAccessLayer import initializeDB
from payBotUs.settings import APPLICATION_SETTINGS

def setup():
    config = APPLICATION_SETTINGS
    NLPBot.initialize(config["watson"])
    DataStore.initialize({
        "redis": config["redis"]
    })
    initializeDB(config["db"])
    manager = ComponentManager()
    manager.registerInputProcessor(WebInputProcessor)
    manager.registerBot(TemplateBot)

    manager.registerBot(NLPBot)
    DecisionEngine.initialize()
    manager.registerRuleEngine(DecisionEngine)
    manager.registerOutputProcessor(WebOutputProcessor)
    Program.initialize(manager)
