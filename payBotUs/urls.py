"""payBotUs URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from startApp.webstart import processWebRequest, feedbackByConversation, getChatbotWidgetSetting,addToContext, \
    updateBillerSettings
from startApp.webstart import totalchatsPerDayRequest, totalChatEscalatedLiveAgentRequest, totalUserByDayRequest, \
    totalFeedBackByDayRequest, avgSessionLengthRequest, totalPayBillByDayRequest, totalAutoPayBillByDayRequest
from .setup import setup
setup()
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r"^chatbotEngine", processWebRequest, name="processWebRequest"),
    url(r"^feedback", feedbackByConversation, name="feedbackByConversation"),
    url(r"^settings", getChatbotWidgetSetting, name="getChatbotWidgetSetting"),
    url(r"^addToContext", addToContext, name="addToContext"),
    url(r'^updateBillerSettings', updateBillerSettings, name="updateBillerSettings"),
    url(r'^totalchatsPerDayRequest', totalchatsPerDayRequest, name="totalchatsPerDayRequest"),
    url(r'^totalChatEscalatedLiveAgentRequest', totalChatEscalatedLiveAgentRequest, name="totalChatEscalatedLiveAgentRequest"),
    url(r'^totalUserByDayRequest', totalUserByDayRequest, name="totalUserByDayRequest"),
    url(r'^totalFeedBackByDayRequest', totalFeedBackByDayRequest, name="totalFeedBackByDayRequest"),
    url(r'^avgSessionLengthRequest', avgSessionLengthRequest, name="avgSessionLengthRequest"),
    url(r'^totalPayBillByDayRequest', totalPayBillByDayRequest, name="totalPayBillByDayRequest"),
    url(r'^totalAutoPayBillByDayRequest', totalAutoPayBillByDayRequest, name="totalAutoPayBillByDayRequest")
]
