import logging

LOGGER = logging.getLogger(__name__)

# TODO: functions can be static as there is not use of self
class Writelog:
    def info(self, conversationId, message):
        LOGGER.info(
            "ConversationId: %s Result got from %s",
            conversationId, message
        )

    def error(self, conversationId, message, ex):
        LOGGER.error(
            "ConversationId: %s Exception occurd in %s: %s",
            conversationId, message, str(ex)
        )
