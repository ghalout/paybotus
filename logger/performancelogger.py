import datetime
import logging

LOGGER = logging.getLogger(__name__)


class Performancelogger:
    def __init__(self):
        self.startTime = datetime.datetime.now()

    @staticmethod
    def start():
        return Performancelogger()

    def stop(self, conversationId, message):
        stopTime = datetime.datetime.now()
        totalTime = stopTime - self.startTime
        LOGGER.info(
            "Converstaion_Id: %s %s Time Elapsed : %s ",
            conversationId, message, str(totalTime)

        )
