import re
from store import DataStore
def parseData(message, additionalData):
    emails = []
    numbers = []
    emails = re.findall(r'\S+@\S+', message)
    message = replaceEmails(message, emails)
    numbers = re.findall('[0-9]+', message)
    message = replaceNumbers(message, numbers, additionalData)
    return message


def replaceEmails(message, emails):
    outPut = message
    for email in emails:
        outPut = outPut.replace(email, 'demo@gmail.com')
    return outPut


def replaceNumbers(message, numbers, additionalData):
    outPut = message
    for number in numbers:
        outPut = lengthCheck(number, outPut, additionalData)
    return outPut


def lengthCheck(number, outPut, additionalData):
    if len(number) == 3:
        outPut = outPut.replace(number, '111')
    elif len(number) == 4:
        outPut = outPut.replace(number, '1111')
    elif len(number) == 5:
        outPut = outPut.replace(number, '11111')
    elif len(number) == 6:
        outPut = outPut.replace(number, '111111')

    elif len(number) == 7:
        initialContext = DataStore.getContext(additionalData["tla"], additionalData["conversationId"])
        dataDict = {"accountNumber": number}
        initialContext.update(dataDict)
        DataStore.setContext(
            additionalData["tla"],
            additionalData["conversationId"],
            initialContext
        )
        outPut = outPut.replace(number, '1111111')
    elif len(number) == 8:
        outPut = outPut.replace(number, '11111111')

    elif len(number) == 9:
        outPut = outPut.replace(number, '111111111')
    elif len(number) == 10:
        outPut = outPut.replace(number, '1111111111')
    elif len(number) == 15:
        outPut = outPut.replace(number, '111111111111111')
    elif len(number) == 16:
        outPut = outPut.replace(number, '1111111111111111')
    return outPut
