import datetime
import logging
from PaxcelChatBotFramework import OutputProcessor
from dataAccessLayer.dbHandler import insertChatlogs
from store import DataStore
from dataParse.dataParseForLogsAndWatson import parseData
from .webResponse import WebResponse
from dataParse.dataParseForLogsAndWatson import parseData
LOGGER = logging.getLogger(__name__)


class WebOutputProcessor(OutputProcessor):

    @staticmethod
    def CanHandle(request) -> bool:
        return request.AdditionalData["source"] == "web"

    def Process(self, request):
        try:
            additionalData = request.AdditionalData
            response = WebResponse()
            response.Data = {
                "Output": request.Output,
                "AdditionalData": additionalData
            }
            response.Context = request.Context

            self.addConversationLog(response)

            DataStore.setContext(
                additionalData["tla"],
                response.Context.ConversationId,
                response.Context.__json__()
            )

        except Exception as ex:
            LOGGER.info("User Query: %s %s",
                        "Exception occurred in output processor", str(ex)
                        )
            raise
        return response

    def addConversationLog(self, response):
        botMessage = self.formatLogMessage(response)
        timeStart = datetime.datetime.utcnow()
        data = response.Data
        #tla = data["AdditionalData"]["tla"]
        for userMessage in botMessage:
            logData = {
                "conversationId" : response.Context.ConversationId,
                "inputMessage" :parseData(userMessage, data["AdditionalData"]),
                "startTime" : timeStart,
                "isBot" : True,
                "dialogName" : ""
            }
            insertChatlogs(logData)

    def formatLogMessage(self, response):
        outputJson = response.__json__()
        botMessage = []
        for data in outputJson["data"]:
            if "option" in data["type"]:
                botMessage.append(data["data"]["text"])
            elif "text" in data["type"]:
                botMessage.append(data["message"])
            else:
                botMessage.append("Form is rendered")
        return botMessage
