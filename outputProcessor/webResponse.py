from PaxcelChatBotFramework.Models import Response

class WebResponse(Response):
    def __json__(self):
        baseResp = super().__json__()
        resp = {
            "conversationId": self.Context.ConversationId,
            "data": baseResp["data"]
        }
        return resp
