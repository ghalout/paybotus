"""
Data store act as the central repo for the available data
"""
import json
import uuid
import redis
from dataAccessLayer import getBillerSettings

class DataStore:
    redisInstance = None
    catalog_url = None
    CONV_PRE = "conv_"
    BILLER_ID_PRE = "blr_"

    @staticmethod
    def initialize(config):
        redisConfig = config.get('redis', {})
        DataStore.redisInstance = redis.Redis(
            host=redisConfig.get('host', 'localhost'),
            port=redisConfig.get('port', 6379),
            decode_responses=True
        )

    @staticmethod
    def generateConversationId():
        return uuid.uuid4().hex

    @staticmethod
    def setContext(billerId, convId, context):
        convId = DataStore.generateConvHashKey(billerId, convId)
        DataStore.redisInstance.hset(
            name="context",
            key=convId,
            value=json.dumps(context)
        )

    @staticmethod
    def getContext(billerId, convId):
        convHashKey = DataStore.generateConvHashKey(billerId, convId)
        value = DataStore.redisInstance.hget(
            name="context",
            key=convHashKey
        )

        if value is None:
            billerSettings = getBillerSettings(billerId)
            if 'name' in billerSettings:
                billerName = billerSettings['name']
            context = {
                "conversationId": convId,
                "tla": billerId,
                "billerName": billerName
            }
            DataStore.setContext(billerId, convId, context)
        else:
            context = json.loads(value)
        return context

    @staticmethod
    def getBillerSetting(billerId):
        hashKey = DataStore.generateBillerHashKey(billerId)
        value = DataStore.redisInstance.hget(
            name="setting",
            key=hashKey
        )
        if value is None:
            setting = DataStore.updateBillerSetting(billerId)
        else:
            setting = json.loads(value)
        return setting

    @staticmethod
    def updateBillerSetting(billerId):
        hashKey = DataStore.generateBillerHashKey(billerId)
        setting = DataStore.loadBillerSetting(billerId)
        DataStore.redisInstance.hset(
            name="setting",
            key=hashKey,
            value=json.dumps(setting)
        )
        return setting

    @staticmethod
    def getBillerChatbotSetting(billerId):
        setting = DataStore.getBillerSetting(billerId)
        chatbotSetting = setting["chatbot"]
        return chatbotSetting

    @staticmethod
    def loadBillerSetting(billerId):
        setting = getBillerSettings(billerId)
        return setting

    @staticmethod
    def generateBillerHashKey(billerId):
        return DataStore.BILLER_ID_PRE + str(billerId)

    @staticmethod
    def generateConvHashKey(billerId, convId):
        return "{0}{1}_{2}".format(DataStore.CONV_PRE, str(billerId), str(convId))
