import datetime
import logging
import json
import requests
from PaxcelChatBotFramework import Program
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, JsonResponse
from dataAccessLayer.dbHandler import insertFeedback
from logger.performancelogger import Performancelogger
from store import DataStore
from logAnalytics.analytics import getCHATLOGDataByDate, getFeedbackDataByDate, totalchatsPerDay, \
    totalChatEscalatedLiveAgent
from logAnalytics.analytics import totalUserByDay, totalFeedbackByDay, \
    avgSessionLengthbyDate, payBillPerDay, getPayTrackDataByDate, autoPayBillPerDay
from payBotUs.settings import APPLICATION_SETTINGS

LOGGER = logging.getLogger(__name__)


@csrf_exempt
def processWebRequest(request):
    performanceLogger = Performancelogger.start()
    requestDict = dict(json.loads(request.body))
    #requestDict["additionalData"]["headers"] = dict(request.headers)
    requestDict["additionalData"]["headers"] = \
        {"Accept": "application/json, text/plain, */*", "Content-Type": "text/plain", \
            "User-Agent":""}
    outputDict = Program().Process(requestDict)
    jsonOutput = json.dumps(
        outputDict.__json__(), sort_keys=True, indent=2, separators=(",", ": ")
    )

    result = HttpResponse(
        jsonOutput, content_type="application/json", charset="UTF-8")
    performanceLogger.stop(outputDict.Context.ConversationId, "backend")
    return result


@csrf_exempt
def feedbackByConversation(request):
    start = datetime.datetime.now()

    request = dict(json.loads(request.body))
    tla = request["additionalData"]["tla"]
    if "conversationId" not in request["additionalData"]:
        newConv = DataStore.generateConversationId()
        request["additionalData"]["conversationId"] = newConv
    covnersationId = request["additionalData"]["conversationId"]
    feedback = request["input"]["text"]
    logData = {}
    logData["conversationId"] = covnersationId
    logData["user"] = tla
    logData["inputMessage"] = feedback
    logData["startTime"] = start
    insertFeedback(logData)
    result = HttpResponse(
        request, content_type="application/json", charset="UTF-8")
    stop = datetime.datetime.now()
    totalTime = "Total time used by backend :" + str(stop - start)
    LOGGER.info("User Query: %s", totalTime)
    return result


@csrf_exempt
def getChatbotWidgetSetting(request):
    request = dict(json.loads(request.body))
    billerId = request["tla"]

    url = APPLICATION_SETTINGS["widgetSetting"]
    headers = {
        "biller": billerId
    }

    response = requests.post(url, headers=headers)
    settings = response.json()
    jsonOutput = json.dumps(
        settings)
    result = HttpResponse(
        jsonOutput, content_type="application/json", charset="UTF-8")

    return result


@csrf_exempt
def addToContext(request):
    request = dict(json.loads(request.body))
    initialContext = DataStore.getContext(request["tla"], request["convId"])
    dataDict = {k: v for k, v in request["data"].items()}
    initialContext.update(dataDict)
    DataStore.setContext(
        request["tla"],
        request["convId"],
        initialContext
    )
    #value = DataStore.getContext(request["tla"], request["convId"])
    result = HttpResponse(
        "OKAY DONE", content_type="application/json", charset="UTF-8")

    return result


@csrf_exempt
def updateBillerSettings(request):
    body = dict(json.loads(request.body))
    secretKey = body["secret_key"]
    if APPLICATION_SETTINGS['update_setting_key'] != secretKey:
        return HttpResponse("Invalid sceret")
    tla = body['tla']
    DataStore.updateBillerSetting(tla)
    return HttpResponse("OK")


def extractDate(body):
    start = body["startDate"]
    date = start.split("-")
    end = body["endDate"]
    edate = end.split("-")
    startDate = datetime.datetime(int(date[0]), int(date[1]), int(date[2]))
    endDate = datetime.datetime(int(edate[0]), int(edate[1]), int(edate[2]))
    return startDate, endDate


@csrf_exempt
def totalchatsPerDayRequest(request):
    try:
        body = dict(json.loads(request.body))
        startDate, endDate = extractDate(body)
        data = getCHATLOGDataByDate(startDate, endDate)
        result = totalchatsPerDay(data)
        jsonOutput = json.dumps(result)
        result1 = HttpResponse(
            jsonOutput, content_type="application/json", charset="UTF-8", status=200)
        return result1
    except BaseException as ex:
        error = 'Error! Type: {c}, Message: {m}'.format(c=type(ex).__name__, m=str(ex))
    return JsonResponse({'error' : error}, status=500)


@csrf_exempt
def totalChatEscalatedLiveAgentRequest(request):
    try:
        body = dict(json.loads(request.body))
        startDate, endDate = extractDate(body)
        data = getCHATLOGDataByDate(startDate, endDate)
        result = totalChatEscalatedLiveAgent(data)
        jsonOutput = json.dumps(result)
        result1 = HttpResponse(
            jsonOutput, content_type="application/json", charset="UTF-8", status=200)
        return result1
    except BaseException as ex:
        error = 'Error! Type: {c}, Message: {m}'.format(c=type(ex).__name__, m=str(ex))
    return JsonResponse({'error' : error}, status=500)


@csrf_exempt
def totalUserByDayRequest(request):
    try:
        body = dict(json.loads(request.body))
        startDate, endDate = extractDate(body)
        data = getCHATLOGDataByDate(startDate, endDate)
        result = totalUserByDay(data)
        jsonOutput = json.dumps(result)
        result1 = HttpResponse(
            jsonOutput, content_type="application/json", charset="UTF-8", status=200)
        return result1
    except BaseException as ex:
        error = 'Error! Type: {c}, Message: {m}'.format(c=type(ex).__name__, m=str(ex))
    return JsonResponse({'error' : error}, status=500)


@csrf_exempt
def totalFeedBackByDayRequest(request):
    try:
        body = dict(json.loads(request.body))
        startDate, endDate = extractDate(body)
        data = getFeedbackDataByDate(startDate, endDate)
        result = totalFeedbackByDay(data)
        jsonOutput = json.dumps(result)
        result1 = HttpResponse(
            jsonOutput, content_type="application/json", charset="UTF-8", status=200)
        return result1
    except BaseException as ex:
        error = 'Error! Type: {c}, Message: {m}'.format(c=type(ex).__name__, m=str(ex))
    return JsonResponse({'error' : error}, status=500)


@csrf_exempt
def totalPayBillByDayRequest(request):
    try:
        body = dict(json.loads(request.body))
        startDate, endDate = extractDate(body)
        data = getPayTrackDataByDate(startDate, endDate)
        result = payBillPerDay(data)
        jsonOutput = json.dumps(result)
        result1 = HttpResponse(
            jsonOutput, content_type="application/json", charset="UTF-8", status=200)
        return result1
    except BaseException as ex:
        error = 'Error! Type: {c}, Message: {m}'.format(c=type(ex).__name__, m=str(ex))
    return JsonResponse({'error' : error}, status=500)


@csrf_exempt
def totalAutoPayBillByDayRequest(request):
    try:
        body = dict(json.loads(request.body))
        startDate, endDate = extractDate(body)
        data = getPayTrackDataByDate(startDate, endDate)
        result = autoPayBillPerDay(data)
        jsonOutput = json.dumps(result)
        result1 = HttpResponse(
            jsonOutput, content_type="application/json", charset="UTF-8", status=200)
        return result1
    except BaseException as ex:
        error = 'Error! Type: {c}, Message: {m}'.format(c=type(ex).__name__, m=str(ex))
    return JsonResponse({'error' : error}, status=500)


@csrf_exempt
def avgSessionLengthRequest(request):
    try:
        body = dict(json.loads(request.body))
        startDate, endDate = extractDate(body)
        data = avgSessionLengthbyDate(startDate, endDate)
        jsonOutput = json.dumps(data)
        result1 = HttpResponse(
            jsonOutput, content_type="application/json", charset="UTF-8", status=200)
        return result1
    except BaseException as ex:
        error = 'Error! Type: {c}, Message: {m}'.format(c=type(ex).__name__, m=str(ex))
    return JsonResponse({'error' : error}, status=500)
