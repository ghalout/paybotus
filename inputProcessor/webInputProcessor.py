import logging
import json
from PaxcelChatBotFramework import InputProcessor
from PaxcelChatBotFramework.Models import Request
from store import DataStore
from dataAccessLayer.dbHandler import insertChatlogsMaster
from dataParse.dataParseForLogsAndWatson import parseData

LOGGER = logging.getLogger(__name__)


class WebInputProcessor(InputProcessor):

    @staticmethod
    def CanHandle(request) -> bool:

        return request["additionalData"]["source"] == "web"

    def Process(self, request):
        try:
            LOGGER.info(
                "User Query: step1 %s",
                json.dumps(request)
            )

            additionalData = request['additionalData']

            conversationId = self.getConversationId(additionalData)
            billerSetting = self.getBillerSetting(additionalData)
            # Merge all the dict into a single one
            additionalData = {
                **additionalData,
                **billerSetting,
                "conversationId": conversationId
            }

            if "message" in request['type']:
                inputMessage = request['input']['message']
                inputMessage = parseData(inputMessage, additionalData)
                request["input"]["message"] = inputMessage
            else:
                inputMessage = request['input']['data']['formName']

            # TODO: remove this after db changes
            additionalData["message"] = {
                "message": inputMessage
            }
            request = {
                "type": request["type"],
                "context": DataStore.getContext(additionalData["tla"], conversationId),
                "additionalData": additionalData,
                "input": request["input"]
            }
            result = Request(request)

        except Exception as ex:
            LOGGER.error(
                "User Query: %s : %s",
                "an exception occurd in web input processor",
                str(ex)
            )
            raise
        return result
    @classmethod
    def getBillerSetting(cls, requestInfo):
        billerId = requestInfo["tla"]
        billerSettings = DataStore.getBillerChatbotSetting(billerId)
        return billerSettings

    @classmethod
    def getConversationId(cls, requestInfo):
        if "conversationId" in requestInfo:
            conversationId = requestInfo["conversationId"]
        else:
            conversationId = DataStore.generateConversationId()
            logData = {
                "conversationId" : conversationId,
                "tla" : requestInfo["tla"],
                "user_agent" : requestInfo["headers"]["User-Agent"],
                "ip_address" : "",
                "headers" : requestInfo["headers"],
                "browser" : "",
                "os" : "",
                "visitor" : {
                    "firstName" : "",
                    "lastName" : ""
                }
            }
            insertChatlogsMaster(logData)
        return conversationId
