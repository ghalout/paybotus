import logging
import pandas as pd
from dataAccessLayer.dbConnectionPooling import getDb

LOGGER = logging.getLogger(__name__)


def getCHATLOGDataByDate(startDate, endDate):
    collection = getDb()["CHATLOGS"]
    logs = collection.find({"added_on": {"$gte": startDate, "$lt":endDate}})
    myList = []
    for data in logs:
        strValue = str(data["added_on"])
        myDate = strValue.split(" ")
        data["added_on"] = myDate[0]
        myList.append(data)
    return myList

def avgSessionLengthbyDate(startDate, endDate):
    collection = getDb()["CHATLOGS"]
    data = collection.aggregate([{"$match" : {"added_on" : \
        {"$gte" : startDate, "$lte" : endDate}}},\
        {"$project" : {"date" : {"$dateToString" : {"format" : "%Y-%m-%d", \
        "date" : "$added_on"}}, "added_on" : "$added_on", "conv_id" : "$conv_id"}}, \
        {"$group" : {"_id" : {"on_date" : "$date", "conv_id" : "$conv_id"}, \
        "firstDate" : {"$first" : "$added_on"}, "lastDate" : {"$last" : "$added_on"}}},\
        {"$project" : {"_id" : 1, "sub" : {"$subtract" : ["$lastDate", "$firstDate"]}}},\
        {"$group" : {"_id" : "$_id.on_date", "time" : {"$push" : "$sub"}}},\
        {"$project" : {"_id" : 1, "avg_time" : {"$avg" : "$time"}}},\
        {"$project" : {"_id" : 0, "added_on" : "$_id", "avg_time" : \
        {"$divide" : ["$avg_time", 60000]}}},\
        {"$sort" : {"_id" : 1}}])
    myList = []
    for time in data:
        time["avg_time"] = round(abs(time["avg_time"]), 2)
        myList.append(time)
    return myList

def getFeedbackDataByDate(startDate, endDate):
    collection = getDb()["CBT_FEEDBACKS"]
    logs = collection.find({"added_on": {"$gte": startDate, "$lt":endDate}})
    myList = []
    for data in logs:
        strValue = str(data["added_on"])
        myDate = strValue.split(" ")
        data["added_on"] = myDate[0]
        myList.append(data)
    return myList

def getPayTrackDataByDate(startDate, endDate):
    collection = getDb()["CBT_BLR_TRACK"]
    logs = collection.find({"added_on": {"$gte": startDate, "$lt":endDate}})
    myList = []
    for data in logs:
        strValue = str(data["added_on"])
        myDate = strValue.split(" ")
        data["added_on"] = myDate[0]
        myList.append(data)
    return myList

def payBillPerDay(data):
    myData = pd.DataFrame.from_dict(data, orient='columns')
    data = myData.groupby(["added_on"])["payCount"].sum().reset_index(name="payBillCount")
    dict1 = data.to_dict(orient="records")
    return dict1

def autoPayBillPerDay(data):
    myData = pd.DataFrame.from_dict(data, orient='columns')
    data = myData.groupby(["added_on"])["autoPayCount"].apply(lambda x: x[x == True].count())\
        .reset_index(name="autoPayBillCount")
    dict1 = data.to_dict(orient="records")
    return dict1

def totalchatsPerDay(data):
    myData = pd.DataFrame.from_dict(data, orient='columns')
    data = myData.groupby(["added_on"])["message"].count()
    dat = data.reset_index()
    dict1 = dat.to_dict(orient="records")
    return dict1

def totalChatEscalatedLiveAgent(data):
    myData = pd.DataFrame.from_dict(data, orient='columns')
    data = myData.groupby('added_on')['intent_detected'].\
        apply(lambda x: x[x == 'SwitchAgent'].count())
    dat = data.reset_index(name="switchAgent")
    dict1 = dat.to_dict(orient="records")
    return dict1

def totalUserByDay(data):
    myData = pd.DataFrame.from_dict(data, orient='columns')
    data = myData.groupby(["added_on"])["conv_id"].nunique()
    dat = data.reset_index(name="userCount")
    dict1 = dat.to_dict(orient="records")
    return dict1

def totalFeedbackByDay(data):
    myData = pd.DataFrame.from_dict(data, orient='columns')
    data = myData.groupby(["added_on"])["conv_id"].nunique()
    dat = data.reset_index(name="feedbackCount")
    dict1 = dat.to_dict(orient="records")
    return dict1
    